<form action="{{url('proc-update-bab')}}" id="formAktifUser" method="POST" class="form-horizontal">
    @csrf
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-3 control-label">BAB</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control input-circle-left" placeholder="Nama" name="name" value="{{$bab->bab}}">
                    <input type="hidden" name="id_bab" value="{{$bab->id}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-book"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Awal</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control input-circle-left" placeholder="date" name="tgl_awal" value="{{$bab->tgl_awal}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Akhir</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control input-circle-left" placeholder="date" name="tgl_akhir" value="{{$bab->tgl_akhir}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        
        
    </div>
    <div class="modal-footer">
        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Keluar</button>
        <button type="button" onClick="procEditBab()"  class="btn green">Simpan</button>
    </div>
</form>