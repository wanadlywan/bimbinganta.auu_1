
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                    </div>
                </div>
                    <div id="notif_alert" class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> <p id="notif"> You have some form errors. Please check below.</p> 
                    </div>
                
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <!-- <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="items" class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th> BAB </th>
                                    <th> Tgl Awal </th>
                                    <th> Tgl Akhir </th>
                                    <th> Keterangan </th>
                                    <th> Upload </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($prosesSubmit as $val)
                                <tr class="odd gradeX">
                                    <td>
                                        <label class="mt-checkbox mt-checkbox-single sasa mt-checkbox-outline">
                                            <input type="checkbox" class="checkboxes" value="1" />
                                            <span></span>
                                        </label>
                                    </td>
                                    <td> {{$val['bab']}} </td>
                                    <td> {{$val['tgl_awal']}} </td>
                                    <td> {{$val['tgl_akhir']}}</td>
                                    <td class="center"> {{$dropStatus[$val['status']]}} </td>
                                    <td class="center"> {{$val['upload']}}  </td>
                                    <td>
                                         <button type="button" class="btn blue btn-sm mt-ladda-btn ladda-button btn-circle" onClick="detail('{{$val['id_proses']}}',{{$progTa->id}})" data-style="zoom-in">
                                            <span class="ladda-label"><i class="fa fa-list"></i> Bimbingan</span>
                                          </button>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
   
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal Title</h4>
            </div>
            <div class="modal-body" id="content"> 
                <div id="loading">
                    <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                </div>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

function detail(e,x){
    var id_proses = e;
    var id_progTa = x;
// alert(id_dosen_mhs);
    $.ajax({
        type: 'GET',
        url : '{{url('dosen/getInputBimbingan/')}}',
        data: {id_proses:id_proses,id_progTa:id_progTa},
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#loading').hide()
            $('#subContentBimTa').html(data)
        }
    });    
}

function procTa(){
    var token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type: 'POST',
        url : $('#formBimbinganTa').attr('action'),
        data: $('#formBimbinganTa').serialize()+'&token='+token,
        dataType: 'json',
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            alert(data.notif)
            $('#basic').modal('hide')
            $('#loading').hide()
            proses_ta(id_user)
            $('#notif_alert').show()
            $('#notif').html(data.notif)
            $('#notif_alert').addClass(data.alert)
        }
    });    
}
</script>
