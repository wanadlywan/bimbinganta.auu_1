<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                        </li>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab">Study Info</a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                        </li>
                        <!-- <li>
                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                        </li> -->
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- PERSONAL INFO TAB -->
                        <div class="tab-pane active" id="tab_1_1">
                            <form role="form" id="formUserDetail" action="{{url('user/proc-user-profile')}}" method="POST">
                                <div class="form-group">
                                    <label class="control-label">Nama Depan</label>
                                    <input type="text" name="first_name" placeholder="first name" value="{{$user->first_name}}" class="form-control" />
                                    <input type="hidden" name="id_user" value="{{$id_user}}" placeholder="first name" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nama Belakang</label>
                                    <input type="text" name="last_name" placeholder="last name" value="{{$user->last_name}}" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Tanggal Lahir</label>
                                    <input type="date" name="birth_date" placeholder="" value="{{$user->birth_date}}" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Nomor Telephone</label>
                                    <input type="text" name="no_phone" placeholder="+62 98789878 DEMO (6284)" value="{{$user->no_phone}}" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Alamat</label>
                                    <textarea type="text" name="address" class="form-control" >{{$user->address}} </textarea></div>
                               
                                <div class="margiv-top-10">
                                    <button type="button" onClick="procUserDetail()" class="btn green"> Simpan </button>
                                </div>
                            </form>
                        </div>
                        <!-- END PERSONAL INFO TAB -->
                        <div class="tab-pane" id="tab_1_3">
                            <form role="form" id="formUserDetail2" action="{{url('user/proc-user-profile')}}" method="POST">
                                @if(Auth::user()->role == 1)
								<div class="form-group">
                                    <label class="control-label">No AK</label>
                                    <input type="text" name="nik" placeholder="No AK" value="{{$user->nik}}" class="form-control" />
                                    <input type="hidden" name="id_user" value="{{$id_user}}" placeholder="NIK" class="form-control" />
                                    <input type="hidden" name="flag" value="1" placeholder="first name" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Program Studi</label>
                                    <input type="text" name="prodi" placeholder="Program Studi" value="{{$user->prodi}}" class="form-control" /> </div>
									@endif
                                @if(Auth::user()->role == 2)
									<div class="form-group">
                                    <label class="control-label">NRP</label>
                                    <input type="text" name="nik" placeholder="NRP" value="{{$user->nik}}" class="form-control" />
                                    <input type="hidden" name="id_user" value="{{$id_user}}" placeholder="NIK" class="form-control" />
                                    <input type="hidden" name="flag" value="1" placeholder="first name" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Korps</label>
                                    <input type="text" name="prodi" placeholder="Korps" value="{{$user->prodi}}" class="form-control" /> </div>
									@endif
                                @if(Auth::user()->role == 1)
                                <div class="form-group">
                                    <label class="control-label">Judul TA</label>
                                    <input type="text" name="judulTa" placeholder="Judul TA" value="{{$user->judulTa}}" class="form-control" /> </div>
                                @endif
                                <div class="margiv-top-10">
                                    <button type="button" onClick="procUserDetail2()" class="btn green"> Simpan </button>
                                </div>
                            </form>
                        </div>
                        <!-- CHANGE AVATAR TAB -->
                        <div class="tab-pane" id="tab_1_2">
                            <p> Upload foto profil anda </p>
                            <form action="{{url('user/proc-user-profile-upload')}}" role="form" id="formUserDetailUpld">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <!-- <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />  -->
                                            @if(!empty($user->img))
                                            <img src="{{url('uploads/dokumen/Usr_'.Auth::user()->id.'/'.$user->img)}}" alt="" /> 
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="..." id="upldFoto"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10">
                                    <a href="javascript:;" id="upldFotoSbmt" class="btn green"> Submit </a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE AVATAR TAB -->
                        <!-- CHANGE PASSWORD TAB -->
                        <!-- <div class="tab-pane" id="tab_1_3">
                            <form action="{{url('user/proc-user-profile-password')}}" role="form" method="POST" id="formUserDetailPass">
                            @csrf
                                <div class="form-group">
                                    <label class="control-label">Current Password</label>
                                    <input type="password" name="old_password" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" name="password" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password</label>
                                    <input type="password" name="password_confirmation" class="form-control" /> </div>
                                <div class="margin-top-10">
                                    <button type="submit" class="btn green"> Change Password </button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div> -->
                        <!-- END CHANGE PASSWORD TAB -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function procUserDetail(){
        var token = $('meta[name="csrf-token"]').attr('content')
        $.ajax({
            type: 'POST',
            url : $('#formUserDetail').attr('action'),
            data: $('#formUserDetail').serialize()+'&_token='+token,
            dataType: 'JSON',
            beforeSend:function(){
                $('#loadings').show()
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#loadings').hide()
                    
            },
            success: function(data){
                alert(data.notif)
                $('#loadings').hide()
                $('#notif_alert').show()
                $('#notif').html(data.notif)
                $('#notif_alert').addClass(data.alert)
                userProfile()
            }
        }); 
    }

    function procUserDetail2(){
        var token = $('meta[name="csrf-token"]').attr('content')
        $.ajax({
            type: 'POST',
            url : $('#formUserDetail2').attr('action'),
            data: $('#formUserDetail2').serialize()+'&_token='+token,
            dataType: 'JSON',
            beforeSend:function(){
                $('#loadings').show()
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#loadings').hide()
                    
            },
            success: function(data){
                alert(data.notif)
                $('#loadings').hide()
                $('#notif_alert').show()
                $('#notif').html(data.notif)
                $('#notif_alert').addClass(data.alert)
                userProfile()
                location.reload();
            }
        }); 
    }

    $('#upldFotoSbmt').click(function(){
        var token = $('meta[name="csrf-token"]').attr('content')
        var fd = new FormData();
        var files = $('#upldFoto')[0].files[0];
        fd.append('upload',files);
        fd.append('_token',token);

        $.ajax({
            type: 'POST',
            url : $('#formUserDetailUpld').attr('action'),
            data: fd,
            dataType: 'JSON',      
            processData: false,
            contentType: false,
            beforeSend:function(){
                $('#loadings').show()
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#loadings').hide()
                    
            },
            success: function(data){
                alert(data.notif)
                $('#loadings').hide()
                $('#notif_alert').show()
                $('#notif').html(data.notif)
                $('#notif_alert').addClass(data.alert)
                userProfile()
                location.reload();
            }
        }); 
    })
</script>