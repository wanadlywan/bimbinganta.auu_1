<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Bimbingan</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <!-- <img src="../assets/layouts/layout3/img/logo-default.jpg" alt="logo" class="logo-default"> -->
                            <h1><b>   PORTAL</h1>
                            <span>TUGAS AKHIR</span></b>
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if(!empty($userDetail->img))
                                    <img alt="" class="img-circle" src="{{url('uploads/dokumen/Usr_'.Auth::user()->id.'/'.$userDetail->img)}}">
                                @endif
                                    <span class="username username-hide-mobile">{{Auth::user()->name}}</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">

                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i>
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>
                                <i class="icon-logout"></i>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>User</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light portlet-fit ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="icon-user font-green"></i>
                                                            <span class="caption-subject bold font-green uppercase"> Data Taruna </span>
                                                            <!-- <span class="caption-helper">user timeline</span> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <!-- PORTLET MAIN -->
                                                            <div class="portlet light profile-sidebar-portlet ">
                                                                <!-- SIDEBAR USERPIC -->
                                                                <div class="profile-userpic">
                                                                    <img src="{{url($images)}}" class="img-responsive" alt=""> </div>
                                                                <!-- END SIDEBAR USERPIC -->
                                                                <!-- SIDEBAR USER TITLE -->
                                                                <div class="profile-usertitle">
                                                                    <div class="profile-usertitle-name"> {{Auth::user()->name}} </div>
                                                                    <div class="profile-usertitle-job"> Taruna </div>
                                                                </div>
                                                                <!-- END SIDEBAR USER TITLE -->
                                                            </div>
                                                            <!-- END PORTLET MAIN -->
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="portlet light profile-sidebar-portlet ">
                                                                <div class="general-item-list">
                                                                    <div class="item">
                                                                        <div class="item-head">
                                                                            <div class="item-details">
                                                                                <a href="" class="item-name primary-link">Nama </a>
                                                                                <span class="item-label">: {{$userDetail->first_name}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                        <div class="item-head">
                                                                            <div class="item-details">
                                                                                <a href="" class="item-name primary-link">No AK </a>
                                                                                <span class="item-label">: {{$userDetail->nik}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                        <div class="item-head">
                                                                            <div class="item-details">
                                                                                <a href="" class="item-name primary-link">Prodi </a>
                                                                                <span class="item-label">: {{$userDetail->prodi}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                        <div class="item-head">
                                                                            <div class="item-details">
                                                                                <a href="" class="item-name primary-link">Judul TA </a>
                                                                                <span class="item-label">: {{$userDetail->judulTa}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="portlet light ">
                                                                <div class="portlet-title tabbable-line">
                                                                    <div class="caption">
                                                                        <i class="icon-share font-dark"></i>
                                                                        <span class="caption-subject font-dark bold uppercase">Menu Navigasi</span>
                                                                    </div>
                                                                    <ul class="nav nav-tabs">
                                                                        <li class="active"> 
                                                                            <a href="#portlet_tab1" data-toggle="tab"> Dashboard </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#portlet_tab2" onClick="proses_ta({{Auth::user()->id}})" data-toggle="tab"> Progress TA </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#portlet_tab3" onClick="bimbingan_ta({{Auth::user()->id}})" data-toggle="tab"> Bimbingan TA </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#portlet_tab4" onClick="userProfile()" data-toggle="tab"> Profile </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane active" id="portlet_tab1">
                                                                            <div class="row">
                                                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                    <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                                                                        <div class="visual">
                                                                                            <i class="fa fa-comments"></i>
                                                                                        </div>
                                                                                        <div class="details">
                                                                                            <div class="number">
                                                                                                <span data-counter="counterup" data-value="1449">@if(!empty($totalBTa)) {{$totalBTa}} @endif</span>
                                                                                            </div>
                                                                                            <div class="desc"> Total Bimbingan </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                    <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                                                                        <div class="visual">
                                                                                            <i class="fa fa-bar-chart-o"></i>
                                                                                        </div>
                                                                                        <div class="details">
                                                                                            <div class="number">
                                                                                                <span data-counter="counterup" data-value="12,5">@if(!empty($persenPTa)) {{$persenPTa}} @endif</span>% </div>
                                                                                            <div class="desc"> Progress Laporan </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                    <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                                                                        <div class="visual">
                                                                                            <i class="fa fa-shopping-cart"></i>
                                                                                        </div>
                                                                                        <div class="details">
                                                                                            <div class="number">
                                                                                                <span data-counter="counterup" data-value="">@if(!empty($progPTa)) {{$progPTa}} @endif</span>%
                                                                                            </div>
                                                                                            <div class="desc"> Progress Program </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                                <div class="portlet light bordered">
                                                                                    <div class="portlet-body">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6">
                                                                                                <!--begin: widget 1-1 -->
                                                                                                <div class="mt-widget-1">
                                                                                                    <div class="mt-icon">
                                                                                                        <a href="#">
                                                                                                            <i class="icon-plus"></i>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="mt-img">
                                                                                                    @if(!empty($userMhs->id_dosen1))
                                                                                                    <img src="{{url('uploads/dokumen/Usr_'.$userMhs->id_dosen1.'/'.$user_img[$userMhs->id_dosen1])}}" width="90" height="90"> 
                                                                                                    
                                                                                                    </div>
                                                                                                    <div class="mt-body">
                                                                                                        <h3 class="mt-username">{{$dropUser[$userMhs->id_dosen1]}}</h3>
                                                                                                        <p class="mt-user-title"> Dosen Pembimbing 1 </p>
                                                                                                        <div class="mt-stats">
                                                                                                            <div class="btn-group btn-group btn-group-justified">
                                                                                                                <a href="javascript:;" class="btn font-red">
                                                                                                                    <i class="icon-bubbles"></i>  </a>
                                                                                                                <a href="javascript:;" class="btn font-green">
                                                                                                                    <i class="icon-social-twitter"></i>  </a>
                                                                                                                <a href="javascript:;" class="btn font-yellow">
                                                                                                                    <i class="icon-emoticon-smile"></i>  </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    @endif
                                                                                                </div>
                                                                                                <!--end: widget 1-1 -->
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <!--begin: widget 1-3 -->
                                                                                                <div class="mt-widget-1">
                                                                                                    <div class="mt-icon">
                                                                                                        <a href="#">
                                                                                                            <i class="icon-plus"></i>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="mt-img">
                                                                                                    @if(!empty($userMhs->id_dosen2))
                                                                                                        <img src="{{url('uploads/dokumen/Usr_'.$userMhs->id_dosen2.'/'.$user_img[$userMhs->id_dosen2])}}" width="90" height="90"> 
                                                                                                    </div>
                                                                                                    <div class="mt-body">
                                                                                                        <h3 class="mt-username">{{$dropUser[$userMhs->id_dosen2]}}</h3>
                                                                                                        <p class="mt-user-title"> Dosen Pembimbing 2 </p>
                                                                                                        <div class="mt-stats">
                                                                                                            <div class="btn-group btn-group btn-group-justified">
                                                                                                                <a href="javascript:;" class="btn font-yellow">
                                                                                                                    <i class="icon-bubbles"></i>  </a>
                                                                                                                <a href="javascript:;" class="btn font-red">
                                                                                                                    <i class="icon-social-twitter"></i>  </a>
                                                                                                                <a href="javascript:;" class="btn font-green">
                                                                                                                    <i class="icon-emoticon-smile"></i>  </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    @endif
                                                                                                </div>
                                                                                                <!--end: widget 1-3 -->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <div class="tab-pane" id="portlet_tab2">
                                                                            <div id="subContentTa">
                                                                               
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane" id="portlet_tab3">
                                                                            <div id="subContentBimTa">
                                                                               
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane" id="portlet_tab4">
                                                                            <div id="userProfileMhs">
                                                                               
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- BEGIN PRE-FOOTER -->
        <div class="page-prefooter">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                        <h2>Tentang</h2>
                        <p> Aplikasi Bimbingan Tugas akhir </p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                        <h2>Subscribe Email</h2>
                        <div class="subscribe-form">
                            <form action="javascript:;">
                                <div class="input-group">
                                    <input type="text" placeholder="mail@email.com" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn" type="submit">Submit</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                        <h2>Contacts</h2>
                        <address class="margin-bottom-40"> Phone: 085885979021
                            <br> Email:
                            <a href="#">paksigia@gmail.com</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <div id="loadings" style="display:none">
            <div style="background-color: grey; z-index: 1000000;filter: alpha(opacity=60); opacity: 0.6; width: 100%; top: 0px; left: 0px; position: fixed; height: 100%;"></div>
            <img style="margin: auto; top: 50%; z-index: 100000000; left: 47%; font-size: medium; vertical-align: middle; position: fixed; text-align: center; height: 100px;"
            src="../assets/apps/img/loading.gif" />
        </div>

        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script src="../assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/timeline.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        
<script>
function proses_ta(id){
    id_user = id
    $.ajax({
        type: 'GET',
        url : '{{url('proses-ta/')}}',
        data: {id:id},
        dataType: 'html',
        beforeSend:function(){
            $('#loadings').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loadings').hide()
                
        },
        success: function(data){
            $('#loadings').hide()
            $('#subContentTa').html(data)
        }
    });    
}
function bimbingan_ta(id){
    id_user = id
    $.ajax({
        type: 'GET',
        url : '{{url('bimbingan-ta/')}}',
        data: {id:id},
        dataType: 'html',
        beforeSend:function(){
            $('#loadings').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loadings').hide()
                
        },
        success: function(data){
            $('#loadings').hide()
            $('#subContentBimTa').html(data)
        }
    })
}
function userProfile(){
    var id = {{Auth::user()->id}}
    $.ajax({
        type: 'GET',
        url : '{{url('user/user-profile/')}}',
        data: {id:id},
        dataType: 'html',
        beforeSend:function(){
            $('#loadings').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loadings').hide()
                
        },
        success: function(data){
            $('#loadings').hide()
            $('#userProfileMhs').html(data)
        }
    })
}
</script>
        
    </body>

</html>