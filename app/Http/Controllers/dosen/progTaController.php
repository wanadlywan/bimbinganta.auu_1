<?php

namespace App\Http\Controllers\dosen;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\proses_ta;
use App\tbl_dosen_mhs as DosenMhs;
use App\mst_bab as BAB;
use Auth;
use App\User;

class progTaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user(){
        $data['title'] = 'Proggress TA';
        $id_user = Auth::user()->id;
        $data['list_mhs'] = DosenMhs::where('id_dosen1',$id_user)->orWhere('id_dosen2',$id_user)->get();
        $user_name = User::select('id','name')->get();
        foreach($user_name as $val){
            $data['user_name'][0] = '-';
            $data['user_name'][$val->id] = $val->name;
        }
        $data['dropStatus'] = ["1"=>"Approved","0"=>"Not Approved","2"=>"on Proggress","3"=>"Waiting"];
        return view('user.dosen.data_mhs',$data);
    }

    public function index(Request $request)
    {
        $id_user = Auth::user()->id;
        $data['title'] = 'Proggress TA';
        $id_dosen_mhs = DosenMhs::where('id_mhs',$request->id_mhs)->first(); 
        $progTa = $data['progTa'] = proses_ta::where('id_dosen_mhs',$id_dosen_mhs->id)->first(); 

        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        // dd($data['prosesSubmit']);
        $data['dropStatus'] = ["1"=>"Approved","0"=>"Not Approved","2"=>"on Proggress","3"=>"Waiting"];
        return view('user.dosen.progTa.index',$data);
    }

    public function getModalProgTa(Request $request){
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $id_dosen_mhs = DosenMhs::find($progTa->id_dosen_mhs);
        $data['id_mhs'] = $id_dosen_mhs->id_mhs;
        $data['id_dosen1'] = $id_dosen_mhs->id_dosen1;
        $data['id_dosen2'] = $id_dosen_mhs->id_dosen2;
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        foreach($data['prosesSubmit'] as $val){
            if($val['id_proses'] == $request->id_proses){
                $data['proses'] = $val;
            }
        }
        $user_name = User::select('id','name')->get();
        foreach($user_name as $val){
            $data['user_name'][0] = '-';
            $data['user_name'][$val->id] = $val->name;
        }
        
        return view('user.dosen.progTa.modal',$data);
    }

    public function procProggresTa(Request $request){
        $id_cek = Auth::user()->id;
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $id_dosen_mhs = DosenMhs::find($progTa->id_dosen_mhs);
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        foreach($data['prosesSubmit'] as $key => $val){
            if($val['id_proses'] == $request->id_proses){
                $setKey = $key;
                $proses= $val;
                unset($data['prosesSubmit'][$key]);
            }
        }

        if($id_cek == $id_dosen_mhs->id_dosen1){
            if($proses['status'] == 3){
                $proses['status'] = 1;
                $proses['nilaiDosen1'] = $request->nilai;
            }else{
                $result = ['notif'=>'Dosen Pembimbing 2 blm Menyetujui dokumen ini','alert'=>'alert-warning'];
                return $result;
            }
        }elseif($id_cek == $id_dosen_mhs->id_dosen2){
            if($proses['status'] != 1){
                $proses['status'] = 3;
                $proses['nilaiDosen2'] = $request->nilai;
            }else{
                $result = ['notif'=>'Dosen Pembimbing 2 sudah Menyetujui dokumen ini','alert'=>'alert-warning'];
                return $result;
            }
        }
        


        $data['prosesSubmit'][$setKey] = $proses;
        sort($data['prosesSubmit']);
    
        $dataInsert = json_encode($data['prosesSubmit']);
        $update = proses_ta::find($request->id_progTa)->update([
            'proses_submit'  => $dataInsert,
        ]);

        if($update){
            $result = ['notif'=>'Berhasil Menyimpan data','alert'=>'alert-success'];
        }else{
            $result = ['notif'=>'Gagal Menyimpan data','alert'=>'alert-danger'];
        }

        return $result;
        
    }

    public function addKomen(Request $request){
        // echo 'aa';die;
        $id_cek = Auth::user()->id;
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $id_dosen_mhs = DosenMhs::find($progTa->id_dosen_mhs);
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        foreach($data['prosesSubmit'] as $key => $val){
            if($val['id_proses'] == $request->id_proses){
                $setKey = $key;
                $proses= $val;
                unset($data['prosesSubmit'][$key]);
            }
        }
        if($id_cek == $id_dosen_mhs->id_dosen1){
            $proses['komend1'] = $request->komen;
        }elseif($id_cek == $id_dosen_mhs->id_dosen2){
            $proses['komend2'] = $request->komen;
        }

        $data['prosesSubmit'][$setKey] = $proses;
        sort($data['prosesSubmit']);
    
        $dataInsert = json_encode($data['prosesSubmit']);
        $update = proses_ta::find($request->id_progTa)->update([
            'proses_submit'  => $dataInsert,
        ]);

        if($update){
            $result = ['notif'=>'Berhasil Menyimpan data','alert'=>'alert-success'];
        }else{
            $result = ['notif'=>'Gagal Menyimpan data','alert'=>'alert-danger'];
        }

        return $result;
    }
}
