<?php

namespace App\Http\Controllers\dosen;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\proses_ta;
use App\tbl_dosen_mhs as DosenMhs;
use App\User;
use Auth;

class bimbinganTaController extends Controller
{
    //
    public function user(){
        $data['title'] = 'Bimbingan TA';
        $id_user = Auth::user()->id;
        $data['list_mhs'] = DosenMhs::where('id_dosen1',$id_user)->orWhere('id_dosen2',$id_user)->get();
        $user_name = User::select('id','name')->get();
        foreach($user_name as $val){
            $data['user_name'][0] = '-';
            $data['user_name'][$val->id] = $val->name;
        }
        $data['dropStatus'] = ["1"=>"Approved","0"=>"Not Approved","2"=>"on Proggress","3"=>"Waiting"];
        return view('user.dosen.data_mhsBim',$data);
    }
    public function index(Request $request){
        $data['title'] = 'Bimbingan TA';
        $id_dosen_mhs = DosenMhs::where('id_mhs',$request->id_mhs)->first(); 
        $progTa = $data['progTa'] = proses_ta::where('id_dosen_mhs',$id_dosen_mhs->id)->first(); 
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        $data['dropStatus'] = ["1"=>"Approved","0"=>"Not Approved","2"=>"on Proggress","3"=>"Waiting"];
        return view('user.dosen.bimbinganTa.index',$data);
    }

    public function getInputBimbingan(Request $request){
        $data['title'] = 'Bimbingan TA';
        $id_proses = $data['id_proses'] = $request->id_proses;
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        $data['dropRole'] = ["1"=>"Mahasiswa","2"=>"Dosen"];
        $user = User::all();
        foreach($user as $value){
            $data['dropUser'][$value->id] = $value->name;
        }
        foreach($data['prosesSubmit'] as $val){
            if($val['id_proses'] == $request->id_proses){
                $data['proses'] = $val;
            }
        }
        $prosesBimbingan = json_decode($progTa->proses_bimbingan,true);
        if(!empty($prosesBimbingan)){
            foreach($prosesBimbingan as $keys => $vals){
                if($vals['id_proses'] == $id_proses){
                    $data['prosesBimbingan'][] = $vals;
                }
            }
        }
        return view('user.dosen.bimbinganTa.input_bimbingan',$data);
    }

    public function procBimbinganTa(Request $request){
        $id_cek = Auth::user()->id;
        $progTa = proses_ta::find($request->id_progTa);
        $data['prosesBimbingan'] = json_decode($progTa->proses_bimbingan,true);
        if(isset($data['prosesBimbingan'])){
            $indexBimbingan = $data['prosesBimbingan'];
        }else{
            $indexBimbingan = array();
        }

        $arryBimbingan = [  
            'id_proses' => $request->id_proses,        
            'id_user'   => $id_cek,
            'kode'      => $request->kode,
            'bimbingan' => $request->bimbingan,
            'tanggal'   => date('d-m-Y'),
        ];
        array_push($indexBimbingan, $arryBimbingan);
        $proses = $indexBimbingan;
        
        $dataInsert = json_encode($proses);
        $update = proses_ta::find($request->id_progTa)->update([
            'proses_bimbingan'  => $dataInsert,
        ]);

        if($update){
            $result = ['notif'=>'Berhasil Menyimpan data','alert'=>'alert-success'];
        }else{
            $result = ['notif'=>'Gagal Menyimpan data','alert'=>'alert-danger'];
        }

        return $result;
    }

    public function DeleteBimbingan(Request $request){
        $index = $request->id;
        $progTa = proses_ta::find($request->id_progTa);
        $data['prosesBimbingan'] = json_decode($progTa->proses_bimbingan,true);
        if(isset($data['prosesBimbingan'][$request->id_proses][$index])){
            unset($data['prosesBimbingan'][$request->id_proses][$index]);
            $dataInsert = json_encode($data['prosesBimbingan']);
            $update = proses_ta::find($request->id_progTa)->update([
                'proses_bimbingan'  => $dataInsert,
            ]);
            if($update){
                $result = ['notif'=>'Berhasil Menghapus data','alert'=>'alert-success'];
            }else{
                $result = ['notif'=>'Gagal Menghapus data','alert'=>'alert-danger'];
            }
        }else{
            $result = ['notif'=>'Gagal Menghapusin data','alert'=>'alert-danger'];
        }
        return $result;
    }
}
