<?php

namespace App\Http\Controllers;

use App\proses_ta;
use Illuminate\Http\Request;

class ProsesTaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\proses_ta  $proses_ta
     * @return \Illuminate\Http\Response
     */
    public function show(proses_ta $proses_ta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\proses_ta  $proses_ta
     * @return \Illuminate\Http\Response
     */
    public function edit(proses_ta $proses_ta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\proses_ta  $proses_ta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, proses_ta $proses_ta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\proses_ta  $proses_ta
     * @return \Illuminate\Http\Response
     */
    public function destroy(proses_ta $proses_ta)
    {
        //
    }
}
