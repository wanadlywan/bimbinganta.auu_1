<?php

namespace App\Http\Controllers\mhs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\proses_ta;
use App\User;
use App\tbl_dosen_mhs as DosenMhs;
use App\mst_bab as BAB;
use Auth;

class progTaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data['title'] = 'Proggress TA';
        $id_dosen_mhs = DosenMhs::where('id_mhs',$request->id)->first(); 
        $progTa = $data['progTa'] = proses_ta::where('id_dosen_mhs',$id_dosen_mhs->id)->first(); 
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        $data['dropStatus'] = ["1"=>"Approved","0"=>"Not Approved","2"=>"on Proggress","3"=>"Waiting"];
        return view('user.mahasiswa.progTa.index',$data);
    }

    public function getModalProgTa(Request $request){
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $id_dosen_mhs = DosenMhs::find($progTa->id_dosen_mhs);
        $data['id_mhs'] = $id_dosen_mhs->id_mhs;
        $data['id_dosen1'] = $id_dosen_mhs->id_dosen1;
        $data['id_dosen2'] = $id_dosen_mhs->id_dosen2;
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        foreach($data['prosesSubmit'] as $val){
            if($val['id_proses'] == $request->id_proses){
                $data['proses'] = $val;
            }
        }
        $user_name = User::select('id','name')->get();
        foreach($user_name as $val){
            $data['user_name'][0] = '-';
            $data['user_name'][$val->id] = $val->name;
        }
        return view('user.mahasiswa.progTa.modal',$data);
    }

    public function procProggresTa(Request $request){
        $id_cek = Auth::user()->id;
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        // die(dd($data['prosesSubmit']));
        foreach($data['prosesSubmit'] as $key => $val){
            if($val['id_proses'] == $request->id_proses){
                $setKey = $key;
                $proses= $val;
                unset($data['prosesSubmit'][$key]);
            }
        }
        $proses['komen'] = $request->komen;
        // die(dd($request->file('upload')));
        if($file = $request->hasFile('upload')){    
            $file = $request->file('upload');
            $folderName = 'Usr_'.$id_cek;

            if(!is_dir('./uploads/dokumen/'.$folderName)){
                mkdir('./uploads/dokumen/'.$folderName,0777);
            }
            
            $filename = str_random(5).'_'.$id_cek.'_'.$file->getClientOriginalName();
            $destinationPath = public_path() . "/uploads/dokumen/".$folderName;
            $file->move($destinationPath, $filename);

            $proses['upload'] = $filename;
            
        }

        $data['prosesSubmit'][$setKey] = $proses;
        sort($data['prosesSubmit']);
    
        $dataInsert = json_encode($data['prosesSubmit']);
        $update = proses_ta::find($request->id_progTa)->update([
            'proses_submit'  => $dataInsert,
        ]);

        if($update){
            $result = ['notif'=>'Berhasil Menyimpan data','alert'=>'alert-success'];
        }else{
            $result = ['notif'=>'Gagal Menyimpan data','alert'=>'alert-danger'];
        }

        return $result;
        
    }

    public function addKomen(Request $request){
        $id_cek = Auth::user()->id;
        $progTa = $data['progTa'] = proses_ta::find($request->id_progTa);
        $data['prosesSubmit'] = json_decode($progTa->proses_submit,true);
        // die(dd($data['prosesSubmit']));
        foreach($data['prosesSubmit'] as $key => $val){
            if($val['id_proses'] == $request->id_proses){
                $setKey = $key;
                $proses= $val;
                unset($data['prosesSubmit'][$key]);
            }
        }
        $proses['komen'] = $request->komen;

        $data['prosesSubmit'][$setKey] = $proses;
        sort($data['prosesSubmit']);
    
        $dataInsert = json_encode($data['prosesSubmit']);
        $update = proses_ta::find($request->id_progTa)->update([
            'proses_submit'  => $dataInsert,
        ]);

        if($update){
            $result = ['notif'=>'Berhasil Menyimpan data','alert'=>'alert-success'];
        }else{
            $result = ['notif'=>'Gagal Menyimpan data','alert'=>'alert-danger'];
        }

        return $result;
    }
}
