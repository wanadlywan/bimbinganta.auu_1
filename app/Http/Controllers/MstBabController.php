<?php

namespace App\Http\Controllers;

use App\mst_bab;
use Illuminate\Http\Request;

class MstBabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mst_bab  $mst_bab
     * @return \Illuminate\Http\Response
     */
    public function show(mst_bab $mst_bab)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mst_bab  $mst_bab
     * @return \Illuminate\Http\Response
     */
    public function edit(mst_bab $mst_bab)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mst_bab  $mst_bab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mst_bab $mst_bab)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mst_bab  $mst_bab
     * @return \Illuminate\Http\Response
     */
    public function destroy(mst_bab $mst_bab)
    {
        //
    }
}
