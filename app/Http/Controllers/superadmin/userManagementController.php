<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\user_detail as UserDetail;
use Auth;


class userManagementController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'User Management';
        $data['user'] = User::where('role',3)->get();

        return view('superadmin.userManagement.index',$data);
    }

    public function procSuperUser(Request $request){
        $insert = new User;
        $insert->name = $request['name'];
        $insert->email = $request['email'];
        $insert->role = $request['role'];
        $insert->status = 0;
        $insert->password = Hash::make($request['password']);
        $insert->save();
        $data = $insert;

        if(isset($data->id)){
            $ins_detail = new UserDetail;
            $ins_detail->id_user = $insert->id;
            $ins_detail->save();
        }

        return $data; 
    }
}
