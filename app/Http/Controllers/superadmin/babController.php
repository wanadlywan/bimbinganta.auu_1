<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\mst_bab as BAB;

class babController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['title']  = 'Master BAB';
        $data['no']  = 1;
        $data['bab'] = BAB::all();
     
        return view('superadmin.master.bab.index',$data);
    }
    public function getModal(Request $request){
        $data['bab'] = BAB::find($request->id);
        return view('superadmin.master.bab.modal',$data);
    }
    public function procUpdateBab(Request $request){
        $update = BAB::find($request->id_bab)->update([
            'tgl_awal'  => $request->tgl_awal,
            'tgl_akhir'  => $request->tgl_akhir
          ]); 
          
          if($update){
              $result = ['notif'=>'Data berhasil di update','alert'=>'alert-success'];
              return json_encode($result);
          }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
