<?php

namespace App\Http\Controllers;

use App\tbl_dosen_mhs;
use Illuminate\Http\Request;

class TblDosenMhsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tbl_dosen_mhs  $tbl_dosen_mhs
     * @return \Illuminate\Http\Response
     */
    public function show(tbl_dosen_mhs $tbl_dosen_mhs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tbl_dosen_mhs  $tbl_dosen_mhs
     * @return \Illuminate\Http\Response
     */
    public function edit(tbl_dosen_mhs $tbl_dosen_mhs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tbl_dosen_mhs  $tbl_dosen_mhs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tbl_dosen_mhs $tbl_dosen_mhs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tbl_dosen_mhs  $tbl_dosen_mhs
     * @return \Illuminate\Http\Response
     */
    public function destroy(tbl_dosen_mhs $tbl_dosen_mhs)
    {
        //
    }
}
