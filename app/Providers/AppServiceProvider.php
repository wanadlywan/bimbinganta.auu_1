<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\mst_menu as Menu;
use Auth;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View()->composer('*', function ($view){
            if (Auth::check() == 'true') {
                $role = Auth::user()->role;
                $data['menu'] = Menu::cekRole($role);

                foreach($data['menu'] as $key => $val){
                    if(isset($val->id_parent)){
                        $submenu[] = $val;

                    }
                }
                if(isset($submenu)){
                    $data['sub_menu'] = $submenu;
                }
                View::share($data);
            }

        });
    }
}
