<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(isset(Auth::user()->id)){
        return redirect('home');
    }else{
        return redirect('login');
    }
});

Auth::routes();

Route::get('/coba', function(){
    return view('error.aktifUser');
});


Route::get('/home', 'HomeController@index')->name('home');

$router->group(['middleware' => 'auth'], function () use ($router){
    //menu aktif user
    $router->get('aktif-user','superadmin\aktifuserController@index');
    $router->get('getModalUser','superadmin\aktifuserController@getModalUser');
    $router->get('user-management','superadmin\userManagementController@index');
    $router->post('regis_su','superadmin\userManagementController@procSuperUser');
    $router->post('proc-aktif-user','superadmin\aktifuserController@procUserMhs');
    //menu bab
    $router->get('bab','superadmin\babController@index');
    $router->get('getModalBab','superadmin\babController@getModal');
    $router->post('proc-update-bab','superadmin\babController@procUpdateBab');
    
    //Mahasiswa 
    //progress TA
    $router->get('proses-ta/','mhs\progTaController@index');
    $router->get('getModalProgTa','mhs\progTaController@getModalProgTa');
    $router->post('proc-proggres-ta','mhs\progTaController@procProggresTa');
    $router->get('add-komen','mhs\progTaController@addKomen');
    //bimbingan TA
    $router->get('bimbingan-ta/','mhs\bimbinganTaController@index');
    $router->get('getInputBimbingan','mhs\bimbinganTaController@getInputBimbingan');
    $router->post('proc-bimbingan-ta','mhs\bimbinganTaController@procBimbinganTa');
    $router->post('delete-bimbingan','mhs\bimbinganTaController@DeleteBimbingan');

    //Dosen
    //progress TA
    $router->get('dosen/proses-ta/','dosen\progTaController@index');
    $router->get('dosen/getModalProgTa','dosen\progTaController@getModalProgTa');
    $router->post('dosen/proc-proggres-ta','dosen\progTaController@procProggresTa');
    $router->get('dosen/user','dosen\progTaController@user');
    $router->get('dosen/add-komen','dosen\progTaController@addKomen');
    //bimbingan TA
    $router->get('dosen/bimbingan-ta/','dosen\bimbinganTaController@index');
    $router->get('dosen/getInputBimbingan','dosen\bimbinganTaController@getInputBimbingan');
    $router->post('dosen/proc-bimbingan-ta','dosen\bimbinganTaController@procBimbinganTa');
    $router->post('dosen/delete-bimbingan','dosen\bimbinganTaController@DeleteBimbingan');
    $router->get('dosen/user-bimbingan','dosen\bimbinganTaController@user');
    
    //edit profile
    $router->get('user/user-profile','HomeController@userProfile');
    $router->post('user/proc-user-profile','UserDetailController@store');
    $router->post('user/proc-user-profile-upload','UserDetailController@storeUpload');
    $router->post('user/proc-user-profile-password','UserDetailController@storePassword');

    
});
