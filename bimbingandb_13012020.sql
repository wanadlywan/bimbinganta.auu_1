-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2020 at 03:23 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bimbingandb`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_menu`
--

CREATE TABLE `access_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `access_menu`
--

INSERT INTO `access_menu` (`id`, `id_menu`, `id_role`, `desc`, `created_at`, `updated_at`) VALUES
(1, 1, 3, NULL, NULL, NULL),
(2, 2, 3, NULL, NULL, NULL),
(3, 3, 3, NULL, NULL, NULL),
(4, 4, 6, NULL, NULL, NULL),
(5, 5, 3, NULL, NULL, NULL),
(6, 6, 3, NULL, NULL, NULL),
(7, 7, 6, NULL, NULL, NULL),
(8, 8, 6, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_09_19_011834_create_mst_menus_table', 1),
(9, '2019_09_19_013337_create_access_menus_table', 1),
(10, '2019_09_19_015854_create_user_details_table', 1),
(11, '2019_09_19_160900_create_tbl_dosen_mhs_table', 1),
(12, '2019_09_19_161008_create_proses_tas_table', 1),
(13, '2019_09_19_161123_create_mst_babs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_bab`
--

CREATE TABLE `mst_bab` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_awal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_akhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `mst_bab`
--

INSERT INTO `mst_bab` (`id`, `bab`, `tgl_awal`, `tgl_akhir`, `created_at`, `updated_at`) VALUES
(1, 'BAB I', '01-08-2019', '31-08-2019', NULL, '2020-01-08 18:10:11'),
(2, 'BAB II', '15-08-2019', '14-09-2019', NULL, '2020-01-08 18:10:32'),
(3, 'BAB III', '01-09-2019', '30-09-2019', NULL, '2020-01-08 18:11:01'),
(4, 'BAB IV', '08-1-2019', '21-12-2019', NULL, '2020-01-08 18:11:22'),
(5, 'BAB V', '21-12-2019', '21-01-2020', NULL, '2020-01-08 18:11:43');

-- --------------------------------------------------------

--
-- Table structure for table `mst_menu`
--

CREATE TABLE `mst_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `mst_menu`
--

INSERT INTO `mst_menu` (`id`, `name`, `link`, `icon`, `title`, `id_parent`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'setting', '', 'fa fa-gear', 'Setting', NULL, 2, NULL, NULL),
(2, 'Master', '', 'fa fa-list', 'Master', NULL, 3, NULL, NULL),
(3, 'BAB', '/bab', 'fa fa-list', 'BAB', 2, 1, NULL, NULL),
(4, 'Menu', '/menu', 'fa fa-list', 'Menu', 1, 1, NULL, NULL),
(5, 'User Management', '/user-management', 'fa fa-user', 'User Management', 1, 2, NULL, NULL),
(6, 'Aktivasi User', '/aktif-user', 'fa fa-user', 'Aktivasi User', NULL, 4, NULL, NULL),
(7, 'Master Role', '/mst-role', 'fa fa-list', 'Master Role', 2, 1, NULL, NULL),
(8, 'Dashboard', '/home', 'fa fa-home', 'Dashboard', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `proses_ta`
--

CREATE TABLE `proses_ta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_dosen_mhs` int(11) DEFAULT NULL,
  `proses_submit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proses_bimbingan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `proses_ta`
--

INSERT INTO `proses_ta` (`id`, `id_dosen_mhs`, `proses_submit`, `proses_bimbingan`, `created_at`, `updated_at`) VALUES
(1, 1, '[{\"id_proses\":\"IP954-1Bab1\",\"bab\":\"BAB I\",\"tgl_awal\":\"21-07-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP954-1Bab2\",\"bab\":\"BAB II\",\"tgl_awal\":\"10-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP954-1Bab3\",\"bab\":\"BAB III\",\"tgl_awal\":\"21-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP954-1Bab4\",\"bab\":\"BAB IV\",\"tgl_awal\":\"21-09-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP954-1Bab5\",\"bab\":\"BAB V\",\"tgl_awal\":\"21-10-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"}]', NULL, '2019-09-21 09:16:27', '2019-09-21 09:16:27'),
(2, 2, '[{\"id_proses\":\"IP373-2Bab1\",\"bab\":\"BAB I\",\"tgl_awal\":\"21-07-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP373-2Bab2\",\"bab\":\"BAB II\",\"tgl_awal\":\"10-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP373-2Bab3\",\"bab\":\"BAB III\",\"tgl_awal\":\"21-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP373-2Bab4\",\"bab\":\"BAB IV\",\"tgl_awal\":\"21-09-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"},{\"id_proses\":\"IP373-2Bab5\",\"bab\":\"BAB V\",\"tgl_awal\":\"21-10-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\"}]', NULL, '2019-09-21 09:17:20', '2019-09-21 09:17:20'),
(3, 3, '[{\"id_proses\":\"IP756-3Bab1\",\"kode\":\"\",\"bab\":\"BAB I\",\"tgl_awal\":\"02-07-2019\",\"tgl_akhir\":\"22-07-2019\",\"status\":\"0\",\"upload\":\"THgqx_7_Tulips.jpg\",\"komen\":null,\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP756-3Bab2\",\"kode\":\"\",\"bab\":\"BAB II\",\"tgl_awal\":\"23-07-2019\",\"tgl_akhir\":\"11-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP756-3Bab3\",\"kode\":\"\",\"bab\":\"BAB III\",\"tgl_awal\":\"11-08-2019\",\"tgl_akhir\":\"21-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP756-3Bab4\",\"kode\":\"\",\"bab\":\"BAB IV\",\"tgl_awal\":\"11-09-2019\",\"tgl_akhir\":\"21-09-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP756-3Bab5\",\"kode\":\"\",\"bab\":\"BAB V\",\"tgl_awal\":\"11-10-2019\",\"tgl_akhir\":\"21-10-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"}]', NULL, '2019-10-15 22:02:52', '2019-10-15 22:06:57'),
(4, 4, '[{\"id_proses\":\"IP678-4Bab2\",\"kode\":\"\",\"bab\":\"BAB II\",\"tgl_awal\":\"23-07-2019\",\"tgl_akhir\":\"11-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP678-4Bab3\",\"kode\":\"\",\"bab\":\"BAB III\",\"tgl_awal\":\"11-08-2019\",\"tgl_akhir\":\"21-08-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP678-4Bab4\",\"kode\":\"\",\"bab\":\"BAB IV\",\"tgl_awal\":\"11-09-2019\",\"tgl_akhir\":\"21-09-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP678-4Bab5\",\"kode\":\"\",\"bab\":\"BAB V\",\"tgl_awal\":\"11-10-2019\",\"tgl_akhir\":\"21-10-2019\",\"status\":\"0\",\"upload\":\"\",\"komen\":\"\",\"komend1\":\"\",\"komend2\":\"\"},{\"id_proses\":\"IP678-4Bab1\",\"kode\":\"\",\"bab\":\"BAB I\",\"tgl_awal\":\"02-07-2019\",\"tgl_akhir\":\"22-07-2019\",\"status\":1,\"upload\":\"N2OjR_14_Angela B. Shiflet, George W. Shiflet-Introduction to Computational Science_ Modeling and Simulation for the Sciences-Princeton University Press (2014).pdf\",\"komen\":\"tes2\",\"komend1\":\"perbaiki lagi\",\"komend2\":\"\",\"nilaiDosen2\":\"10\",\"nilaiDosen1\":\"9\"}]', '[{\"id_proses\":\"IP678-4Bab1\",\"id_user\":14,\"kode\":\"kd01\",\"bimbingan\":\"bimbingan 1\",\"tanggal\":\"04-01-2020\"},{\"id_proses\":\"IP678-4Bab1\",\"id_user\":14,\"kode\":\"kd01\",\"bimbingan\":\"sudah oke pak?\",\"tanggal\":\"04-01-2020\"}]', '2020-01-03 03:12:39', '2020-01-08 18:18:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dosen_mhs`
--

CREATE TABLE `tbl_dosen_mhs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_mhs` int(11) DEFAULT NULL,
  `id_dosen1` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_dosen2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tbl_dosen_mhs`
--

INSERT INTO `tbl_dosen_mhs` (`id`, `id_mhs`, `id_dosen1`, `created_at`, `updated_at`, `id_dosen2`) VALUES
(4, 14, 15, '2020-01-03 03:12:39', '2020-01-03 03:12:39', '5');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `status`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'rexyyy', 'rexyyosua@gmail.com', 3, 1, NULL, '$2y$10$JWl38Ga044w.dSNVtM4vLOOQ3AhUMb7ij40HcGGlk4FCsdzbnCLKC', NULL, '2019-09-20 08:28:31', '2019-09-20 08:28:31'),
(2, 'mahasiswa1', 'asd@asd.asd', 1, 1, NULL, '$2y$10$JWl38Ga044w.dSNVtM4vLOOQ3AhUMb7ij40HcGGlk4FCsdzbnCLKC', NULL, '2019-09-20 08:28:31', '2019-09-21 09:16:27'),
(3, 'mahasiswa2', 'asd@asd.asdds', 1, 1, NULL, '$2y$10$JWl38Ga044w.dSNVtM4vLOOQ3AhUMb7ij40HcGGlk4FCsdzbnCLKC', NULL, '2019-09-20 08:28:31', '2019-09-21 09:17:20'),
(5, 'dosen', 'qwe@asd.asd', 2, 1, NULL, '$2y$10$JWl38Ga044w.dSNVtM4vLOOQ3AhUMb7ij40HcGGlk4FCsdzbnCLKC', NULL, '2019-09-20 08:28:31', '2019-09-20 08:28:31'),
(7, 'rexyyy', 'qwe@qwe.qwe1', 1, 1, NULL, '$2y$10$MemdsHNwrRlkv3jSxGF3yes0R2RKEZEMMCeKPRnLzydMSmeF7LZ3q', NULL, '2019-10-15 21:29:06', '2019-10-15 22:00:34'),
(8, 'cobaMhs', 'qwe@qwe.qwe2', 1, 0, NULL, '$2y$10$5H12Qvd4cGQlj5ECtamEx.805dUYzbMV.TTeBREkVaOriiGWqj6QO', NULL, '2019-10-16 05:16:48', '2019-10-16 05:16:48'),
(10, 'sadsa', 'admin@admin1.com', 3, 0, NULL, '$2y$10$3dpBWzM2kYsfoT60hpQg3eacokffAIspOObCPRni6vJsBLFYjTczW', NULL, '2019-10-24 09:56:55', '2019-10-24 09:56:55'),
(12, 'sadsa', 'admin@admin10.com', 3, 0, NULL, '$2y$10$vcVJOO0G7L5BZe9GOf.ymO8A.2aCFCMx4IHesUByB23cEG48kAqV2', NULL, '2019-10-24 09:57:32', '2019-10-24 09:57:32'),
(14, 'giapaksi', 'giapaksi@yahoo.com', 1, 1, NULL, '$2y$10$tbsp54sxAj.eXPJ0774bAu5l.ppbiC17E4BosOaOP8yJF4jgP5Wly', NULL, '2020-01-03 02:51:10', '2020-01-03 03:12:39'),
(15, 'subagio', 'subagio@yahoo.com', 2, 1, NULL, '$2y$10$nurUPtVZ2CSZsyzBXf3wquxOdDzLD9nc5yhkT1dUNOaNCRVRT20K2', NULL, '2020-01-03 02:55:33', '2020-01-03 02:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judulTa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `id_user`, `first_name`, `last_name`, `address`, `no_phone`, `birth_date`, `created_at`, `updated_at`, `img`, `nik`, `judulTa`, `prodi`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, '2019-09-20 08:22:46', '2019-09-20 08:22:46', NULL, NULL, NULL, NULL),
(2, 2, 'coba namas', 'bana', 'sadsada', '12321', '2019-01-01', '2019-09-20 08:27:09', '2019-10-27 14:26:22', 'CmOF8_2_Koala.jpg', '1010101', 'TEST JUDULS', 'TEST PRODIS'),
(4, 7, NULL, NULL, NULL, NULL, NULL, '2019-10-15 21:29:06', '2019-10-15 21:29:06', NULL, NULL, NULL, NULL),
(5, 8, NULL, NULL, NULL, NULL, NULL, '2019-10-16 05:16:49', '2019-10-16 05:16:49', NULL, NULL, NULL, NULL),
(6, 3, NULL, NULL, NULL, NULL, NULL, '2019-09-20 08:28:31', '2019-09-20 08:28:31', NULL, NULL, NULL, NULL),
(7, 4, NULL, NULL, NULL, NULL, NULL, '2019-09-20 08:28:31', '2019-09-20 08:28:31', NULL, NULL, NULL, NULL),
(8, 5, NULL, NULL, NULL, NULL, NULL, '2019-09-20 08:28:31', '2019-09-20 08:28:31', NULL, NULL, NULL, NULL),
(9, 12, NULL, NULL, NULL, NULL, NULL, '2019-10-24 09:57:32', '2019-10-24 09:57:32', NULL, NULL, NULL, NULL),
(10, 13, NULL, NULL, NULL, NULL, NULL, '2019-10-27 20:29:03', '2019-10-27 20:29:03', NULL, NULL, NULL, NULL),
(11, 14, 'gia', 'paksi', 'dirgantara 3 halim p.k', '085885979021', '1997-09-10', '2020-01-03 02:51:10', '2020-01-03 02:55:00', 'VrQVL_14_IMG_7858.JPG', '2016 357', 'Perancangan Sistem Monitoring Progess Pengerjaan Tugas Akhir Taruna Berbasis Website Di AAU', 'Elektronika'),
(12, 15, 'subagio', NULL, 'Yogyakarta', '081310874235', '1967-01-01', '2020-01-03 02:55:33', '2020-01-03 03:07:01', 'nI6Bs_15_subagio.jpg', '563100', NULL, 'Elektronika');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_menu`
--
ALTER TABLE `access_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mst_bab`
--
ALTER TABLE `mst_bab`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mst_menu`
--
ALTER TABLE `mst_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indexes for table `proses_ta`
--
ALTER TABLE `proses_ta`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_dosen_mhs`
--
ALTER TABLE `tbl_dosen_mhs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_menu`
--
ALTER TABLE `access_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mst_bab`
--
ALTER TABLE `mst_bab`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mst_menu`
--
ALTER TABLE `mst_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `proses_ta`
--
ALTER TABLE `proses_ta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_dosen_mhs`
--
ALTER TABLE `tbl_dosen_mhs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
